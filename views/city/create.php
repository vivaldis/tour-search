<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\City */

$this->title = 'Города > Создать';
$this->params['breadcrumbs'][] = ['label' => 'Города', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Создать';
?>
<div class="city-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
