<?php

use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Direction */

$cityName = $model->getAttribute('city_name');
$countryName = $model->getAttribute('country_name');
$name = "{$cityName} → {$countryName}";

$this->title = "Направления > изменить > {$name}";
$this->params['breadcrumbs'][] = ['label' => 'Направления', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $name, 'url' => ['view', 'city_id' => $model->city_id, 'country_id' => $model->country_id]];
$this->params['breadcrumbs'][] = 'Изменить';
?>
<div class="direction-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
