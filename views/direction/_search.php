<?php

use app\models\Direction;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\DirectionSearch */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="direction-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'city_id')->dropDownList(Direction::listRelations('city'), [
        'prompt' => [
            'text' => 'Любой город',
            'options' => [],
        ],
    ]) ?>

    <?= $form->field($model, 'country_id')->dropDownList(Direction::listRelations('country'), [
        'prompt' => [
            'text' => 'Любая страна',
            'options' => [],
        ],
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton('Поиск', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Отмена', ['class' => 'btn btn-dark']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
