<?php

use yii\bootstrap4\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Direction */

$cityName = $model->getAttribute('city_name');
$countryName = $model->getAttribute('country_name');
$name = "{$cityName} → {$countryName}";

$this->title = "Направления > {$name}";
$this->params['breadcrumbs'][] = ['label' => 'Направления', 'url' => ['index']];
$this->params['breadcrumbs'][] = $name;
\yii\web\YiiAsset::register($this);
?>
<div class="direction-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Изменить', ['update', 'city_id' => $model->city_id, 'country_id' => $model->country_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'city_id' => $model->city_id, 'country_id' => $model->country_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => "Удалить направление {$name}?",
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'city_name',
                'format' => 'html',
                'value' => function ($model, $widget) {
                    return Html::a($model->getAttribute('city_name'), ['city/view', 'id' => $model->city_id]);
                },
            ],
            [
                'attribute' => 'country_name',
                'format' => 'html',
                'value' => function ($model, $widget) {
                    return Html::a($model->getAttribute('country_name'), ['country/view', 'id' => $model->country_id]);
                },
            ],
        ],
    ]) ?>

</div>
