<?php

use app\models\City;
use app\models\Country;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Direction */
/* @var $form yii\bootstrap4\ActiveForm */
?>

<div class="direction-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'city_id')->dropDownList(City::toList(), [
        'prompt' => [
            'text' => 'Выбрать город',
            'options' => [],
        ],
    ])->label($model->getAttributeLabel('city_name')) ?>

    <?= $form->field($model, 'country_id')->dropDownList(Country::toList(), [
        'prompt' => [
            'text' => 'Выбрать страну',
            'options' => [],
        ],
    ])->label($model->getAttributeLabel('country_name')) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
