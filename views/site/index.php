<?php

/* @var $this yii\web\View */
/* @var $model app\models\TourSearchForm */
?>

<?= $this->render('_modal') ?>

<div class="tour-search">

    <?= $this->render('_tour-search-form', [
        'model' => $model,
    ]) ?>

    <?= $this->render('_tour-search-result') ?>

</div>
