<?php

/* @var $this yii\web\View */
?>

<div data-control="tourSearchResult" class="d-none">

    <h2 class="h4 text-center text-muted">Результаты поиска</h2>

    <div class="table-responsive-md">
        <table class="table table-bordered table-hover table-sm table-striped" data-control="tourList">
            <thead class="thead-light text-center">
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Название отеля</th>
                    <th scope="col">Название партнёра</th>
                    <th scope="col">Цена</th>
                </tr>
            </thead>
            <tbody></tbody>
        </table>
    </div>

</div>
