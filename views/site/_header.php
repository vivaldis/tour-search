<?php

/* @var $this yii\web\View */

use yii\bootstrap4\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;

?>
<header>
<?php
    NavBar::begin([
        'options' => [
            //'tag' => 'nav',
            'class' => [
                'navbar-expand-md',
                'navbar-light',
                'bg-light',
            ],
        ],

        'collapseOptions' => [
            //'tag' => 'div',
        ],

        'brandLabel' => Html::encode('Поиск туров'), //false,
        'brandImage' => false,
        'brandUrl' => '/', //false,
        'brandOptions' => [],

        'screenReaderToggleText' => 'Показать/скрыть главное меню', //'Toggle navigation',
        'togglerContent' => '<span class="navbar-toggler-icon"></span>',
        'togglerOptions' => [],

        'renderInnerContainer' => true,
        'innerContainerOptions' => [],

        'clientOptions' => false,
    ]);
?>
    <?= Nav::widget([
        'options' => [
            'class' => [
                'navbar-nav',
                'ml-auto',
            ],
        ],
        'items' => array_map(function($entry) {
            return [
                'label' => $entry[1],
                'url' => ["{$entry[0]}/index"],
                'active' => preg_match("/^{$entry[0]}\/.+$/", Yii::$app->request->resolve()[0]),
            ];
        }, [
            ['country', 'Страны'],
            ['city', 'Города'],
            ['direction', 'Направления'],
        ]),
        'encodeLabels' => true,
        'activateItems' => true,
        'activateParents' => false,
        //'route' => null,
        //'params' => null,
        'dropdownClass' => 'yii\bootstrap4\Dropdown',
    ]) ?>
<?php
    NavBar::end();
?>
</header>
