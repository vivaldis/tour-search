<?php

/* @var $this yii\web\View */
/* @var $model app\models\TourSearchForm */
/* @var $form yii\bootstrap4\ActiveForm */

use app\models\Direction;
use kartik\date\DatePicker;
use kartik\touchspin\TouchSpin;
use yii\bootstrap4\ActiveForm;
use yii\bootstrap4\Html;

?>

<?php $form = ActiveForm::begin([
    'layout' => ActiveForm::LAYOUT_HORIZONTAL,
    'action' => '',
    'method' => 'post',
    'id' => 'tourSearchForm',

    'scrollToError' => false,
    'validateOnBlur' => false,
    'validateOnChange' => false,
    'validationStateOn' => ActiveForm::VALIDATION_STATE_ON_CONTAINER,

    'options' => [],

    'fieldConfig' => function ($model, $attribute) {
        return [
            'horizontalCssClasses' => [
                'label' => 'col-4',
                'wrapper' => 'col-4',
                'error' => 'col-4',
            ],
            'template' => '{label}{beginWrapper}{input}{endWrapper}{error}',
            'inputOptions' => [
                'data' => [
                    'control' => $attribute,
                ],
            ],
        ];
    },
]); ?>

<?= $form->field($model, 'depCity')->dropDownList(Direction::listRelations('city'), [
    'prompt' => [
        'text' => 'Выбрать…',
        'options' => [],
    ],
]) ?>

<?= $form->field($model, 'destCountry')->dropDownList([], [
    'prompt' => [
        'text' => 'Выбрать…',
        'options' => [],
    ],
    'disabled' => true,
]) ?>

<?= $form->field($model, 'depDate')->widget(DatePicker::class, [
    'type' => DatePicker::TYPE_COMPONENT_APPEND, // TYPE_INPUT|TYPE_COMPONENT_PREPEND|TYPE_COMPONENT_APPEND|TYPE_RANGE|TYPE_INLINE|TYPE_BUTTON
    'readonly' => true,
    'layout' => '<div class="input-group-prepend">{picker}</div>{input}{remove}',
    'pickerButton' => [
        'class' => 'd-none d-md-flex text-secondary',
        'style' => 'border-top-right-radius: 0; border-bottom-right-radius: 0;',
    ],
    //'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
    'removeButton' => [
        'class' => 'text-secondary',
    ],
    'removeIcon' => '<i class="far fa-calendar-times"></i>',
    'pluginOptions' => [
        'autoclose' => true,
        'assumeNearbyYear' => 20,
        'disableTouchKeyboard' => true,
        'format' => 'dd.mm.yyyy',
        'todayHighlight' => true,
        'templates' => [
            'leftArrow' => '<i class="fas fa-arrow-left"></i>',
            'rightArrow' => '<i class="fas fa-arrow-right"></i>',
        ],
    ],
    'options' => [
        'class' => 'text-center',
        'data' => [
            'control' => 'depDate',
        ],
    ],
]) ?>

<?= $form->field($model, 'nights')->widget(TouchSpin::class, [
    'readonly' => true,
    'pluginOptions' => [
        'min' => $model::NIGHTS_MIN,
        'max' => $model::NIGHTS_MAX,
        'step' => 1,
        'decimals' => 0,
        'buttonup_txt' => '<i class="fas fa-arrow-right"></i>',
        'buttondown_txt' => '<i class="fas fa-arrow-left"></i>',
    ],
    'options' => [
        'class' => 'text-center',
        'data' => [
            'control' => 'nights',
        ],
    ],
]) ?>

<div class="form-group row no-gutters">
    <?= Html::submitButton('Поиск', ['class' => 'btn btn-secondary col-4']) ?>
</div>

<?php ActiveForm::end(); ?>
