<?php

/* @var $this yii\web\View */

use yii\bootstrap4\Modal;

?>
<?php
    Modal::begin([
        'title' => '',
        'size' => 'modal-dialog-centered', // trick to center modal vertically
        'options' => [
            'data' => [
                'control' => 'modal',
            ],
        ],
    ]);
?>
<?php
    Modal::end();
?>
