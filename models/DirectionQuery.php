<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Direction]].
 *
 * @see Direction
 */
class DirectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $city = City::tableName();
        $country = Country::tableName();
        // explicitly join relation tables to additionally select "related" fields
        // (to reduce the number of queries)
        $this
            ->innerJoin("{$city} AS {$city}", "{$city}_id = {$city}.id")
            ->innerJoin("{$country} AS {$country}", "{$country}_id = {$country}.id")
            ->addSelect([
                '*',
                "{$city}_name" => "{$city}.name",
                "{$country}_name" => "{$country}.name",
            ]);
        parent::init();
    }

    /**
     * {@inheritdoc}
     * @return Direction[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return Direction|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
