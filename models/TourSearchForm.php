<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * TourSearchForm is the model behind the tour search form.
 */
class TourSearchForm extends Model
{
    /**
     * Minimum number of nights
     */
    const NIGHTS_MIN = 5;
    /**
     * Maximum number of nights
     */
    const NIGHTS_MAX = 15;

    /**
     * Departure city
     * @var integer city id
     */
    public $depCity;
    /**
     * Country of arrival
     * @var integer country id
     */
    public $destCountry;
    /**
     * Departure date
     * @var date
     */
    public $depDate;
    /**
     * Number of nights
     * @var integer
     */
    public $nights;

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->depDate = Yii::$app->formatter->asDate(time());
        $this->nights = intval((self::NIGHTS_MIN + self::NIGHTS_MAX) / 2);
        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['depCity', 'destCountry', 'depDate', 'nights'], 'required', 'message' => 'Обязательное поле'],
            [['depCity', 'destCountry'], 'integer', 'skipOnEmpty' => false, 'message' => 'Некорректное значение'],

            // only on the client-side for date to match the DD.MM.YYYY pattern
            [['depDate'], 'match', 'pattern' => '/^\d{2}\.\d{2}\.\d{4}$/', 'skipOnEmpty' => false, 'message' => 'Некорректный формат даты',
                'when' => function ($model, $attribute) { return false; }
            ],
            // server-side
            [['depDate'], 'date', 'skipOnEmpty' => false, 'message' => 'Некорректный формат даты'],

            [['nights'], 'integer', 'min' => self::NIGHTS_MIN, 'max' => self::NIGHTS_MAX, 'skipOnEmpty' => false,
                'message' => 'Значение должно быть целым числом',
                'tooSmall' => 'От ' . self::NIGHTS_MIN . ' дней',
                'tooBig' => 'До ' . self::NIGHTS_MAX . ' дней',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'depCity' => 'Город отправления',
            'destCountry' => 'Страна прибытия',
            'depDate' => 'Дата отправления',
            'nights' => 'Количество ночей',
        ];
    }
}
