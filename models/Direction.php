<?php

namespace app\models;

//use Yii;

/**
 * This is the model class for table "direction".
 *
 * @property int $city_id ид. города
 * @property int $country_id ид. страны
 *
 * @property City $city
 * @property Country $country
 */
class Direction extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'direction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['city_id', 'country_id'], 'required', 'message' => 'Обязательное поле'],
            [['city_id', 'country_id'], 'integer'],
            [['city_id', 'country_id'], 'unique', 'targetAttribute' => ['city_id', 'country_id'], 'message' => 'Такое направление уже существует'],
            [['city_id'], 'exist', 'skipOnError' => true, 'targetClass' => City::className(), 'targetAttribute' => ['city_id' => 'id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributes()
    {
        // add "related" fields to attributes to reduce the number of queries
        // instead of fetching city and country names via relations
        return array_merge(parent::attributes(), ['city_name', 'country_name']);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'city_id' => 'Ид. города',
            'country_id' => 'Ид. страны',
            'city_name' => 'Город',
            'country_name' => 'Страна',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCity()
    {
        return $this->hasOne(City::className(), ['id' => 'city_id'])->inverseOf('directions');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id'])->inverseOf('directions');
    }

    /**
     * {@inheritdoc}
     * @return DirectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new DirectionQuery(get_called_class());
    }

    /**
     * Represents all Direction items as a list of related City or Country items.
     * Useful for dropdown widgets.
     *
     * @param string $relation Either 'city' or 'country'
     * @param array $whereCondition The conditions that should be put in the WHERE part (@see yii\db\QueryInterface::where() for details)
     * @return array Direction items in terms of City or Country relation as an array [id => name]
     */
    public static function listRelations($relation, $whereCondition = [])
    {
        $rel = strtolower(trim($relation));
        if (!in_array($rel, ['city', 'country'], true)) return [];

        $city = City::tableName();
        $country = 'countries';
        if ($rel === 'country') $rel = $country;
        return City::find()
            ->from([$city => $city])
            ->innerJoinWith("{$country} AS {$country}")
            ->select([
                "{$rel}_name" => "{$rel}.name",
                "{$rel}_id" => "{$rel}.id",
            ])
            ->where($whereCondition)
            ->orderBy(["{$rel}.name" => SORT_ASC, "{$rel}.id" => SORT_ASC])
            ->indexBy("{$rel}_id")
            ->column();
    }
}
