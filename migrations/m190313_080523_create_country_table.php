<?php

use yii\db\Migration;

/**
 * Handles the creation of table for countries.
 */
class m190313_080523_create_country_table extends Migration
{
    /**
     * string table name for countries
     */
    const TABLE = 'country';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // create the table
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->comment('ид. страны'),
            'name' => $this->string()->notNull()->comment('название страны'),
        ], 'ENGINE = InnoDB');

        // add comment to the table
        $this->addCommentOnTable(self::TABLE, 'страны');

        // insert predefined countries into the table
        $this->batchInsert(self::TABLE, ['name'], [
            ['Турция'],
            ['Таиланд'],
            ['Чехия']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // delete predefined countries from the table
        $this->delete(self::TABLE, ['between', 'id', 1, 3]);

        // drop comment from the table
        $this->dropCommentFromTable(self::TABLE);

        // drop the table
        $this->dropTable(self::TABLE);
    }
}
