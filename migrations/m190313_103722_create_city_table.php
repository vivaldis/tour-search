<?php

use yii\db\Migration;

/**
 * Handles the creation of table for cities.
 */
class m190313_103722_create_city_table extends Migration
{
    /**
     * string table name for cities
     */
    const TABLE = 'city';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // create the table
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->comment('ид. города'),
            'name' => $this->string()->notNull()->comment('название города'),
        ], 'ENGINE = InnoDB');

        // add comment to the table
        $this->addCommentOnTable(self::TABLE, 'города');

        // insert predefined cities into the table
        $this->batchInsert(self::TABLE, ['name'], [
            ['Алматы'],
            ['Астана'],
            ['Москва']
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // delete predefined cities from the table
        $this->delete(self::TABLE, ['between', 'id', 1, 3]);

        // drop comment from the table
        $this->dropCommentFromTable(self::TABLE);

        // drop the table
        $this->dropTable(self::TABLE);
    }
}
