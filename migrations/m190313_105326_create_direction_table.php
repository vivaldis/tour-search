<?php

use yii\db\Migration;

/**
 * Handles the creation of table for directions.
 * Has foreign keys to the tables:
 *
 * - for cities (`city`)
 * - for countries (`country`)
 */
class m190313_105326_create_direction_table extends Migration
{
    /**
     * string table name for directions
     */
    const TABLE = 'direction';

    /**
     * Column, index and foreign key constraint names for junction tables combined together.
     * @var array ['junctionTable' => ['column' => string, 'indexName' => string, 'fkName' => string]]
     */
    private $junctionNames = [
        'city' => [],
        'country' => [],
    ];

    /**
     * {@inheritdoc}
     */
    public function init() {
        // set default values of column, index and foreign key constraint for junction tables
        foreach ($this->junctionNames as $tbl => &$opts) {
            $col = $opts['column'] = "{$tbl}_id";
            $opts['indexName'] = 'ix-' . self::TABLE . "-{$col}";
            $opts['fkName'] = 'fk-' . self::TABLE . "-{$col}";
        }
        unset($opts);

        parent::init();
    }

    /**
     * Creates index and adds foreign key constraint for the table.
     * 
     * @param string $refTable  The table that the foreign key references to.
     * @param string $refColumn The name of the column that the foreign key references to.
     * @param string $column    The name of the column to that the index and the foreign key constraint will be added on.
     * @param string $indexName The name of the index.
     * @param boolean $indexUnique Whether to add UNIQUE constraint on the created index.
     * @param string $fkName    The name of the foreign key constraint.
     * @param string $fkOnDelete    The ON DELETE option of the foreign key constraint.
     * @param string $fkOnUpdate    The ON UPDATE option of the foreign key constraint.
     */
    private function junctionUp($refTable, $refColumn = 'id', $column = null,
            $indexName = null, $indexUnique = false,
            $fkName = null, $fkOnDelete = 'CASCADE', $fkOnUpdate = 'CASCADE') {
        // set default values for empty params
        foreach (['column', 'indexName', 'fkName'] as $param) {
            if (empty($$param)) $$param = $this->junctionNames[$refTable][$param];
        }

        // creates index for column in the table
        $this->createIndex(
            $indexName,
            self::TABLE,
            $column,
            $indexUnique
        );

        // add foreign key constraint for the table
        $this->addForeignKey(
            $fkName,
            self::TABLE,
            $column,
            $refTable,
            $refColumn,
            $fkOnDelete,
            $fkOnUpdate
        );
    }

    /**
     * Drops index and foreign key constraint for the table.
     * 
     * @param string $indexName The name of the index.
     * @param string $fkName    The name of the foreign key constraint.
     */
    private function junctionDown($indexName, $fkName) {
        // drops foreign key constraint for the table
        $this->dropForeignKey(
            $fkName,
            self::TABLE
        );

        // drops index for column in the table
        $this->dropIndex(
            $indexName,
            self::TABLE
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        // create the table
        $this->createTable(self::TABLE, [
            'city_id' => $this->integer()->notNull()->comment('ид. города'),
            'country_id' => $this->integer()->notNull()->comment('ид. страны'),
        ], 'ENGINE = InnoDB');

        // adjust junction to junction tables
        foreach (['city', 'country'] as $t) $this->junctionUp($t);
        
        // create unique index for the table
        $this->createIndex('ixu-' . self::TABLE, self::TABLE, ['city_id', 'country_id'], true);

        // add comment to the table
        $this->addCommentOnTable(self::TABLE, 'направления');

        // insert predefined directions into the table
        $this->batchInsert(self::TABLE, ['city_id', 'country_id'], [
            [1, 1],
            [1, 2],
            [2, 1],
            [2, 3]
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // delete predefined directions from the table
        $this->delete(self::TABLE, ['and', ['city_id' => 1], ['country_id' => [1, 2]]]);
        $this->delete(self::TABLE, ['and', ['city_id' => 2], ['country_id' => [1, 3]]]);

        // drop comment from the table
        $this->dropCommentFromTable(self::TABLE);
        
        // drop unique index for the table
        $this->dropIndex('ixu-' . self::TABLE, self::TABLE);

        // drop junction to junction tables
        foreach (['city', 'country'] as $t) $this->junctionDown($this->junctionNames[$t]['indexName'], $this->junctionNames[$t]['fkName']);

        // drop the table
        $this->dropTable(self::TABLE);
    }
}
