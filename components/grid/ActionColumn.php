<?php

namespace app\components\grid;

use yii\bootstrap4\Html;

/**
 * Custom adjustment of [[\yii\grid\ActionColumn]]
 * @see \yii\grid\ActionColumn
 */
class ActionColumn extends \yii\grid\ActionColumn
{
    /**
     * {@inheritdoc}
     */
    public $template = '{update} {delete}';

    /**
     * {@inheritdoc}
     */
    public $contentOptions = [
        'class' => 'text-right',
    ];

    /**
     * {@inheritdoc}
     */
    protected function initDefaultButton($name, $iconName, $additionalOptions = [])
    {
        if (!isset($this->buttons[$name]) && strpos($this->template, '{' . $name . '}') !== false) {
            $this->buttons[$name] = function ($url, $model, $key) use ($name, $additionalOptions) {
                $attrData = [];
                switch ($name) {
                    case 'view':
                        $title = 'Просмотр';
                        $btnClass = 'info';
                        break;
                    case 'update':
                        $title = 'Изменить';
                        $btnClass = 'warning';
                        break;
                    case 'delete':
                        $title = 'Удалить';
                        $btnClass = 'danger';
                        $attrData = [
                            'confirm' => isset($model->attributes['name'])
                                ? "Удалить элемент \"{$model->name}\"?"
                                : (isset($model->attributes['id'])
                                    ? "Удалить элемент #{$model->id}?"
                                    : "Удалить указанный элемент?"
                                ),
                            'method' => 'post',
                        ];
                        break;
                    default:
                        $title = ucfirst($name);
                        $btnClass = 'default';
                }
                $options = array_merge([
                    'title' => $title,
                    'data' => array_merge([
                        'pjax' => '0',
                    ], $attrData),
                    'class' => [
                        'btn',
                        'btn-sm',
                        "btn-{$btnClass}",
                    ],
                ], $additionalOptions, $this->buttonOptions);
                return Html::a($title, $url, $options);
            };
        }
    }
}
