<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Main application asset bundle.
 */
class AppAsset extends AssetBundle
{
    //public $basePath = '@webroot';
    //public $baseUrl = '@web';
    public $sourcePath = '@app/assets/dist';
    public $css = [
        'styles/index.css',
    ];
    public $js = [
        'scripts/index.js',
    ];
    /*
    public $publishOptions = [
        'only' => [],
        'except' => [],
        'caseSensitive' => false,
    ];
    */
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap4\BootstrapPluginAsset',
    ];
}
