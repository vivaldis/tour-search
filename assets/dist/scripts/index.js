// IIFE
(function() {
    /**
     * Resets tour list: removes all table rows and hides table container.
     * @return {undefined}
     */
    function resetTourList() {
        var $container = $('[data-control="tourSearchResult"]');
        var $table = $container.find('[data-control="tourList"]');

        // remove all tbody rows
        $table.find('tbody').empty();

        // hide table container
        $container.hide();
    }

    /**
     * Renders tour list with the specified data.
     * @param {array} tourItems array of {"partner": string, "tours": array of {hotelName, tourPrice, currency}}
     * @return {undefined}
     */
    function renderTourList(tourItems) {
        var $container = $('[data-control="tourSearchResult"]');
        var $table = $container.find('[data-control="tourList"]');
        var $tbody = $table.find('tbody');

        // render tours
        for (var i = 0, n = 0; i < tourItems.length; i++) {
            for (var t = 0, partner = tourItems[i].partner, tours = tourItems[i].tours; t < tours.length; t++, n++) {
                var $tr = $("<tr></tr>");

                // col "ordinal"
                $("<th></th>")
                    .text(n + 1)
                    .attr('scope', 'row')
                    .addClass('text-center')
                    .appendTo($tr);
                // col "hotel"
                $("<td></td>")
                    .text(tours[t].hotelName)
                    .addClass('text-left')
                    .appendTo($tr);
                // col "partner"
                $("<td></td>")
                    .text(partner)
                    .addClass('text-center')
                    .appendTo($tr);
                // col "price"
                $("<td></td>")
                    .text('' + tours[t].currency + ' ' + tours[t].tourPrice)
                    .addClass('text-right')
                    .appendTo($tr);

                $tr.appendTo($tbody);
            }
        }

        // display table container
        $container.removeClass('d-none').show();
    }

    /**
     * Shows an alert message.
     * @param {string} title Title to show in alert
     * @param {string} message Message to show in alert
     * @return {undefined}
     */
    function showAlert(title, message) {
        var $modal = $('[data-control="modal"]');
        // set title
        $modal.find('.modal-title').text(title);
        // set text
        $modal.find('.modal-body').text(message);
        // show modal
        $modal.modal('show');
    }

    /**
     * Tour search form handlers.
     */
    $(document).ready(function() {
        var $form = $('#tourSearchForm');

        // jQuery objects for each input control which value is going to be send to the server
        var $depCity = $form.find('[data-control="depCity"]');
        var $destCountry = $form.find('[data-control="destCountry"]');
        var $depDate = $form.find('[data-control="depDate"]');
        var $nights = $form.find('[data-control="nights"]');

        /**
         * Handle select on city.
         * Fetch countries for the selected city.
         */
        $depCity.on('change', function() {
            // clean up country list at first
            $destCountry
                .attr('disabled', true)
                .children()
                    .filter(function(i, e) { return i > 0; })
                    .remove();

            var cityId = parseInt($(this).val(), 10);
            if (!cityId) return;

            // fetch country list for the selected city
            $.ajax('/index.php?r=site%2Fcountries', {
                method: 'POST',
                data: {
                    city_id: cityId
                },
                error: function(/*jqXHR*/ jqXHR, /*String*/ textStatus, /*String*/ errorThrown) {
                    console.error('Failed to fetch countries:', textStatus, errorThrown);
                },
                success: function(/*Anything*/ data, /*String*/ textStatus, /*jqXHR*/ jqXHR) {
                    if (!$.isArray(data)) {
                        console.error('Received data is not an array:', data);
                        return;
                    }
                    // insert fetched countries into the country list
                    for (var i = 0; i < data.length; i++) {
                        $('<option></option>')
                            .val(data[i].id)
                            .text(data[i].name)
                            .appendTo($destCountry);
                    }
                    $destCountry.removeAttr('disabled');
                }
            });
        });

        /**
         * Prevent default form submitting.
         * Pass data to the server via WebSocket.
         */
        $form.on('beforeSubmit', function(event) {
            // create and fill the custom object to be sent to the server
            var formData = {};
            for (var i = 0, $inputs = [$depDate, $depCity, $destCountry, $nights]; i < $inputs.length; i++) {
                formData[$inputs[i].attr('data-control')] = i
                    ? parseInt($inputs[i].val()) // all except `departure date` are numbers
                    : $inputs[i].val().split('.').reverse().join('-'); // send `departure date` as 'yyyy-mm-dd' string
            }

            // create the WebSocket object to send the form data to the server
            var ws = new WebSocket('ws://localhost:9242/tours');
            ws.addEventListener('error', function(event) {
                showAlert('Поиск туров', 'An error occurred on the WebSocket to ' + ws.url);
                ws.close();
            });
            ws.addEventListener('message', function(event) {
                // render tour list with the received data
                var tours = [];
                try {
                    tours = JSON.parse(event.data);
                } catch (e) {}
                renderTourList(tours);
                ws.close();
            });
            ws.addEventListener('open', function(event) {
                resetTourList();
                ws.send(JSON.stringify(formData));
            });

            // prevent form submission as Yii docs say
            return false;
        });
    });
})();
