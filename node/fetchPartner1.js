/**
 * Fetches data from the partner1.
 */

/**
 * Node.js fetch implementation
 */
const fetch = require('node-fetch');

const url = 'https://poedem.kz/test/search/partner1';
const method = 'GET';

/**
 * Fetches the tour list.
 * @param {number} departCity id города вылета из твоей бд
 * @param {number} country id страны из твоей бд
 * @param {string} date дата вылета в формате YYYY-MM-DD
 * @param {number} nights кол-во ночей
 * @return {Promise} A Promise resolving to an array of tour objects in terms of {hotelName, tourPrice, currency}
 */
module.exports = async function(departCity, country, date, nights) {
    // the result object
    let result;

    const params = {
        departCity,
        country,
        date,
        nights
    };

    // make up a query string for GET request
    const urlParts = [];
    for (let p in params) {
        urlParts.push([p, encodeURIComponent(params[p])].join('='));
    }

    try {
        const response = await fetch(`${url}?${urlParts.join('&')}`, {
            method
        });
        result = await response.json();

        // normalize property names of received tour objects
        result.tours = result.tours.map(e => {
            return {
                hotelName: e.hotelName,
                tourPrice: e.price,
                currency: e.currency
            };
        });
        console.log(`Succeeded to fetch from partner #1 with parameters ${JSON.stringify(params)}: ${result.tours.length} items.`);
    } catch (e) {
        result = {
            tours: []
        };
        console.error(`Failed to fetch from partner #1 with parameters ${JSON.stringify(params)}: ${e.message}.`);
    } finally {
        return result.tours;
    }
};
