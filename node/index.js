/**
 * Starts WebSocket server listening to search parameters from the tour search form.
 * Then uses received search parameters to fetch the actual data.
 * And finally sends the fetched data to the client.
 */

const fetch1 = require('./fetchPartner1');
const fetch2 = require('./fetchPartner2');

/**
 * WebSocket
 */
const ws = require('ws');

const [host, port, path] = ['localhost', 9242, '/tours'];

/**
 * WebSocket.Server
 */
const wsServer = new ws.Server({
    host,
    port,
    path
}, () => {
    const addr = wsServer.address();
    console.log('The WebSocket server has been bound on address '
        + (typeof addr === 'string' ? `"${addr}"` : `ws://${addr.address}:${addr.port}${path}`)
        + ` and is listening since ${new Date().toLocaleString()}...`);
});

wsServer
    .on('close', () => {
        console.log(`The WebSocket server was closed at ${new Date().toLocaleString()}.`);
    })
    .on('error', /*Error*/ err => {
        console.error(`An error occurred on the WebSocket server at ${new Date().toLocaleString()}: [${err.name}] ${err.message}.`);
    })
    .on('connection', (/*WebSocket*/ socket, /*http.IncomingMessage*/ request) => {
        socket
            .on('error', /*Error*/ err => {
                console.error(`An error occurred on the client WebSocket at ${new Date().toLocaleString()}: [${err.name}] ${err.message}.`);
            })
            .on('message', /*String|Buffer|ArrayBuffer|Buffer[]*/ data => {
                console.info(`A request received with search criteria at ${new Date().toLocaleString()}: `, data);

                let parsedData = {};
                try {
                    parsedData = JSON.parse(data);
                } catch (e) {
                    console.error(`Failed to parse JSON from the client data: "${data}".`);
                }
                const {depCity, destCountry, depDate, nights} = parsedData;

                // fetch data from the partners
                const partners = [
                    {
                        name: 'Partner One',
                        tours: fetch1(depCity, destCountry, depDate, nights)
                    },
                    {
                        name: 'Partner Two',
                        tours: fetch2(depCity, destCountry, depDate, nights)
                    }
                ];
                // resolve all tour lists to a single tour list to be returned to the client
                Promise.all(partners.map(e => e.tours))
                    .then(/*array of arrays of {hotelName, tourPrice, currency}*/ tourLists => {
                        return tourLists.map((e, i) => {
                            /**
                             * the client will receive
                             * an array of
                             * {
                             *  "partner": string,
                             *  "tours": array of {hotelName, tourPrice, currency}
                             * }
                             */
                            return {
                                partner: partners[i].name,
                                tours: e
                            };
                        });
                    })
                    .catch(err => []) // return an empty tour list in case of an error
                    .then(tours => {
                        // send the ultimate tour list to the client
                        socket.send(JSON.stringify(tours), {
                            compress: false,
                            binary: false,
                            fin: true
                        });
                    });
            });
    });
