/**
 * Fetches data from the partner2.
 */

/**
 * Node.js fetch implementation
 */
const fetch = require('node-fetch');

/**
 * XML to JS/JSON converter and vice versa
 */
const xmljs = require('xml-js');

const url = 'https://poedem.kz/test/search/partner2';
const method = 'POST';

/**
 * Fetches the tour list.
 * @param {number} cityId id города вылета из твоей бд
 * @param {number} countryId id страны из твоей бд
 * @param {string} dateFrom дата вылета в формате YYYY-MM-DD
 * @param {number} nights кол-во ночей
 * @return {Promise} A Promise resolving to an array of tour objects in terms of {hotelName, tourPrice, currency}
 */
module.exports = async function(cityId, countryId, dateFrom, nights) {
    // the result object
    let result;

    const params = {
        cityId,
        countryId,
        dateFrom,
        nights
    };

    // make up a query string for request body
    const urlParts = [];
    for (let p in params) {
        urlParts.push([p, encodeURIComponent(params[p])].join('='));
    }
    const body = urlParts.join('&');

    try {
        const response = await fetch(url, {
            method,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            },
            body
        });

        const xml = await response.text();

        // parse XML string to JS object
        const parsedXml = xmljs.xml2js(xml, {compact: true, textKey: 'text'});
        const parsedTours = parsedXml.response.tours.item.map(e => {
            return {
                hotelName: e.hotel.text,
                tourPrice: e.tourPrice.text,
                currency: e.currency.text
            };
        });

        result = {tours: parsedTours};
        console.log(`Succeeded to fetch from partner #2 with parameters ${JSON.stringify(body)}: ${result.tours.length} items.`);
    } catch (e) {
        result = {
            tours: []
        };
        console.error(`Failed to fetch from partner #2 with parameters ${JSON.stringify(body)}: ${e.message}.`);
    } finally {
        return result.tours;
    }
};
