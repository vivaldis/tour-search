<?php

namespace app\controllers;

use Yii;
use app\models\Direction;
use app\models\TourSearchForm;
use yii\web\Controller;
use yii\web\Response;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'ajax' => [
                'class' => 'yii\filters\AjaxFilter',
                'only' => ['countries'],
                'errorMessage' => 'Принимаются только запросы XMLHttpRequest.',
            ],
            'verbs' => [
                'class' => 'yii\filters\VerbFilter',
                'actions' => [
                    'countries' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $model = new TourSearchForm();

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * Fetches and returns countries for the specified city.
     *
     * @return string Country list in terms of [...{'id': integer, 'name': string}] as JSON
     */
    public function actionCountries()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $city_id = Yii::$app->request->post('city_id') ?? null;
        if (is_null($city_id) || !is_numeric($city_id)) return [];

        $countries = Direction::listRelations('country', ['city.id' => intval($city_id)]);
        return array_map(function($k, $v) { return array('id' => $k, 'name' => $v); }, array_keys($countries), $countries);
    }
}
